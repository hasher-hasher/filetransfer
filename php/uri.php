<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/05/17
 * Time: 12:28
 */
$uri = str_replace("uri=", "", $_SERVER['QUERY_STRING']);
?>

<!DOCTYPE html>
<html>
<header>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
    <!--<script type="text/javascript" src="js/require.js"></script>-->
    <script src="../js/webtorrent/webtorrent.min.js"></script>
    <title>TorTran</title>
</header>
<body>
	<div style="width: 25%;height: 50%;position:absolute;left:50%;top:50%;margin:-11% 0 0 -12.5%;">
        <form id="Form" style="background-color: white; padding: 3%; border-radius: 10px;">
            <div class="form-group">
                <input value="<?php echo $uri; ?>" type="text" data-toogle="tooltip" title="Paste URI here" class="form-control" id="URI" placeholder="Paste URI here">
            </div>
            <input id="startDownloadButton" onclick="torrentDownload()" data-toogle="tooltip" title="This button gives you the power to seed you file. TorTran it!" class="btn btn-primary center-block" style="width: 100%" value="Download it!">
        </form>
        <br>
        <div class="progress">
            <div id="progressBar" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            </div>

        </div>
        <p id="downloadProgress" style="text-align: center;">0% Complete</p>
	</div>

	<!-- Button trigger modal -->
	<!--<input type="image" src="t.jpg" style="display: block;max-width: 6%; max-height: 6%; width: auto; height: auto;" class="btn btn-lg" data-toggle="modal" data-target="#myModal">-->
	<button type="button" style="margin: 0.5%" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    TorTran
	</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel" style="display: block; text-align: center">Welcome to TorTran!</h4>
				</div>
				<div class="modal-body">
					<p>TorTran is the simplest and easiest way to send a whatever-size file through Torrent to anyone you want!</p>
					<p>Just start seeding while our system send an Email with the URI code for your friend to download it!</p>
					<p>They can download your file just clicking on the link in the Email</p>
					<p style="color: red">WARNING: You must keep you browser opened while you friend download the file! you are the seeder, so keep seeding while downloading!</p>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="../js/filesaver/FileSaver.min.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/zip.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/zip-ext.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/zip-fs.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/inflate.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/deflate.js"></script>
<script type="text/javascript" src="../js/zipjs/WebContent/z-worker.js"></script>
<script type="text/javascript" src="../js/jszip/dist/jszip.min.js"></script>
<script type="text/javascript" src="../js/javascript.js"></script>
</html>