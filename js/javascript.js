/**
 * Created by root on 31/05/17.
 */

var client = new WebTorrent()

var contentToTransfer

window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;

$('#InputFile').change(function () {
    contentToTransfer = this.files[0]
    document.getElementById("fileName").value = contentToTransfer.name

});

function startProcedures() {
    torrentSeed();
}

function torrentSeed() {
    document.getElementById("collapseOne").style.textAlign = "center"
    document.getElementById("status").style.textAlign = "justify"
    document.getElementById("collapseOne").style.wordWrap = "break-word"
    var nodes = document.getElementById("collapseOne").children
    for(var i=0; i<nodes.length;i++){
        if(nodes[i].id != "status"){
            nodes[i].style.display = "none"
        }
    }
    document.getElementById("status").innerHTML = "Starting to serve"
    document.getElementById("theTwoCollapser").style.display = "none"
    client.seed(contentToTransfer, function (torrent) {
        document.getElementById("status").innerHTML = "Magnet link generated"
        sendEmail(torrent)
    })
}

function sendEmail(torrentInfo){
    var emailFrom = document.getElementById("InputFrom").value
    var emailTo = document.getElementById("InputTo").value
    document.getElementById("status").innerHTML = "Trying to send email"
    $.ajax({
        method: "POST",
        data: {from: emailFrom, to: emailTo, download_magnet: "http://tortransfer.com/php/uri.php?uri="+torrentInfo.magnetURI},
        url: '../php/uploadToServer.php',
        success: function (obj, textstatus) {
            document.getElementById("status").innerHTML = "Don't close your browser. Now you're seeding. <br> An email with this magnet link was sent to informed email: <br> " + torrentInfo.magnetURI
        }
    })
}

function torrentDownload() {
    if(document.getElementById("theOneCollapser") != null){
        document.getElementById("theOneCollapser").style.display = "none"
    }
    var uriContent = document.getElementById("URI").value
    document.getElementById("downloadProgress").innerHTML = "Connecting to server ..."
    client.add(uriContent, {path: '/'}, function (torrent) {
        torrent.on('download', function () {
            document.getElementById("progressBar").style.width = (torrent.progress * 100) + "%"
            document.getElementById("downloadProgress").innerHTML = Math.round(torrent.progress * 100) + "% Complete"
        })
        torrent.on('done', function () {
            document.getElementById("downloadProgress").innerHTML = "Download Complete!"
            if (torrent.files.length > 1) {
                torrent.files.forEach(function (file) {
                    file.getBlobURL(function (err, url) {
                        if (err) throw err
                        var a = document.createElement('a')
                        a.id = "toDownload"
                        a.download = file.name
                        a.href = url
                        a.textContent = 'Download ' + file.name
                        a.style.display = "none"
                        document.body.appendChild(a)
                        document.getElementById("toDownload").click()
                    })
                })
            } else {
                torrent.files.forEach(function (file) {
                    file.getBlobURL(function (err, url) {
                        if (err) throw err
                        var a = document.createElement('a')
                        a.id = "toDownload"
                        a.download = file.name
                        a.href = url
                        a.textContent = 'Download ' + file.name
                        a.style.display = "none"
                        document.body.appendChild(a)
                        document.getElementById("toDownload").click()
                    })
                })
            }
        })
    })
}